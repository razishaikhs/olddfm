package net.directfn.android.dfm;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;
import androidx.core.content.ContextCompat;


public class CommonMethods {

    public static void setWindow(int color, Activity act) {
        Window window = act.getWindow();
        // window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(act, color));
    }
}