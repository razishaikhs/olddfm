package net.directfn.android.dfm;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Locale;

public class WebViewActivity extends Activity {

    WebView web;

    Dialog dialog;

    String language;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        CommonMethods.setWindow(R.color.dark_blue,this);

        Configuration config = getResources().getConfiguration();
        language = config.locale.getLanguage();

        Log.e("language",language);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablets);

        if (tabletSize) {
            if(language.equals("ar")){
                Locale locale = new Locale("ar");
                config.setLocale(locale);
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                url = "https://auth2.dfm.ae/mobilepopup/newpopup_tablet.aspx?lan=ar";

            }else{
                Locale locale = new Locale("en");
                config.setLocale(locale);
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                url = "https://auth2.dfm.ae/mobilepopup/newpopup_tablet.aspx";
            }

        } else {
            if(language.equals("ar")){
                Locale locale = new Locale("ar");
                config.setLocale(locale);
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                url = "https://auth2.dfm.ae/mobilepopup/newpopup.aspx?lan=ar";

            }else{
                Locale locale = new Locale("en");
                config.setLocale(locale);
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                url = "https://auth2.dfm.ae/mobilepopup/newpopup.aspx";
            }

        }

        dialog = new Dialog(WebViewActivity.this, R.style.TransparentDialog);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        dialog.setContentView(R.layout.loader_layout);
        dialog.setCancelable(true);

        openBrowser();

    }

    public void openBrowser() {

        web = findViewById(R.id.web);
        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                /*Log.e("INN","INN");
                web.loadUrl("javascript:(function() {" +
                        "var parent = document.getElementsByTagName('head').item(0);" +
                        "var style = document.createElement('style');" +
                        "style.type = 'text/css';" +
                        "style.innerHTML = .max-w-md { max-width:1024px!important; }" +
                        "parent.appendChild(style)" +
                        "})()");


                Log.e("web",web.getUrl()+" ");*/

            }
        });


        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        web.getSettings().setLoadWithOverviewMode(true);
        web.getSettings().setAllowFileAccess(true);

        web.getSettings().setDatabaseEnabled(true);
        web.getSettings().setDomStorageEnabled(true);
        web.getSettings().setSaveFormData(false);
        web.getSettings().setAppCacheEnabled(true);
        web.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        web.getSettings().setLoadWithOverviewMode(false);
        web.getSettings().setUseWideViewPort(true);

        web.setWebChromeClient(new ChromeClient());
        
        web.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        web.loadUrl(url);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    public class ChromeClient extends WebChromeClient {

        public void onProgressChanged(WebView view, int newProgress) {

            try {
                if (!dialog.isShowing())
                    dialog.show();


                if (newProgress == 100) {

                    if (dialog.isShowing())
                        dialog.cancel();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && web.canGoBack()) {
            web.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
